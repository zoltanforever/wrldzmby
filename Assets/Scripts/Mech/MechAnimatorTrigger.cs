﻿
public enum MechAnimatorTrigger
{
    WalkForward,
    WalkBackward,
    TurnLeft,
    TurnRight
}