﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class BasicMechController : MonoBehaviour
{
    private int _walkForwardTriggerHash;
    private int _walkBackwardTriggerHash;
    private int _turnLeftTriggerHash;
    private int _turnRightTriggerHash;

    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void Start ()
    {
        _walkForwardTriggerHash = Animator.StringToHash(MechAnimatorTrigger.WalkForward.ToString());
        _walkBackwardTriggerHash = Animator.StringToHash(MechAnimatorTrigger.WalkBackward.ToString());
        _turnLeftTriggerHash = Animator.StringToHash(MechAnimatorTrigger.TurnLeft.ToString());
        _turnRightTriggerHash = Animator.StringToHash(MechAnimatorTrigger.TurnRight.ToString());
    }
	
	private void Update ()
    {
        CheckKeys(KeyCode.W, _walkForwardTriggerHash);
        CheckKeys(KeyCode.S, _walkBackwardTriggerHash);
        CheckKeys(KeyCode.A, _turnLeftTriggerHash);
        CheckKeys(KeyCode.D, _turnRightTriggerHash);
    }

    private void CheckKeys(KeyCode keyCode, int trigger)
    {
        if (Input.GetKeyDown(keyCode))
        {
            _animator.SetBool(trigger, true);
        }

        if (Input.GetKeyUp(keyCode))
        {
            _animator.SetBool(trigger, false);
        }
    }
}
