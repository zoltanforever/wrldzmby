﻿using UnityEngine;

public class FallBehaviour : MonoBehaviour
{
    public float RaycastRange;
    public float FallSpeed;
    public LayerMask GroundLayers;

    private Transform _transform;

    private void Awake()
    {
        _transform = transform;
    }
	
	private void Update ()
    {
	    if(!Physics.Raycast(_transform.position, Vector3.down, RaycastRange, GroundLayers.value))
        {
            _transform.Translate(0, -FallSpeed * Time.deltaTime, 0);
        }
	}
}