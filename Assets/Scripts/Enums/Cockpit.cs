﻿using UnityEngine;
using System.Collections;

public enum Cockpit
{
    Compter,
    Fox,
    Jet,
    JetUpgrade,
    Tractor
}
