﻿using UnityEngine;
using System.Collections;

public enum Shoulders
{
    Box,
    Elf,
    Frame,
    FrameUpgrade,
    MediumFrame,
    MediumFrameUpgrade,
    MediumShield,
    MediumShieldUpgrade,
    Wall
}
