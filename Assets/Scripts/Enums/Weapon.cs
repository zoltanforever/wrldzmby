﻿using UnityEngine;
using System.Collections;

public enum Weapon
{
    NoWeapon,
    Cannon,
    Flamethrower,
    Laser,
    Machinegun,
    Rockets1,
    Rockets2,
    RocketsShoulder,
    Minigun
}
